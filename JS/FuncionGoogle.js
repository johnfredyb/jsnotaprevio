function crearTablaGoogle() {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Name');
    data.addColumn('number', 'Salary');
    data.addColumn('boolean', 'Full Time Employee');
    data.addRows([
        ['Mike', { v: 10000, f: '$10,000' }, true],
        ['Jim', { v: 8000, f: '$8,000' }, false],
        ['Alice', { v: 12500, f: '$12,500' }, true],
        ['Bob', { v: 7000, f: '$7,000' }, true]
    ]);

    var table = new google.visualization.Table(document.getElementById('table_div'));

    table.draw(data, { showRowNumber: true, width: '100%', height: '100%' });
}
function crearTabla(previo, notaP){
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Nombre');
    data.addColumn('number', 'Previo teorico');
    data.addColumn('number', 'Previo Practico');
    data.addColumn('number', 'Bonus Previo');
    data.addColumn('number', 'Nota previo 1');
    // crear las filas 
    data.addRows(1);
    data.setCell(0, 0, previo[0].value);
    data.setCell(0, 1, previo[1].value);
    data.setCell(0, 2, previo[2].value);
    data.setCell(0, 3, previo[3].value);
    data.setCell(0, 4, notaP);

    var table = new google.visualization.Table(document.getElementById('table_div'));

    table.draw(data, { showRowNumber: true, width: '100%', height: '100%' });

}
function crearGrafico(previo,notaP){
    let mensaje = ["Nombre", "Previo Teorico", "Previo Practico", "Bonus Previo","Nota Previo 1"];
    var data = new google.visualization.DataTable();
    data.addColumn("string","Descripción");
    data.addColumn("number","Nota");
    data.addRows(4);
    let n = "";

    for(i = 1; i < previo.lenght; i++){
        n += "\n" + previo[i].value + "-" + mensaje[i];
        data.setCell(i-1, 0, mensaje[i]);
        data.setCell(i-1, 1, previo[i].value);
    }
    alert (n);
    var options = {
        title: 'Notas'
      };

    var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
    chart.draw(data, options);
}