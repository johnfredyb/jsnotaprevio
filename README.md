# Primer JavaScript 
***
## Índice
1. [Características](#Características:)
2. [Contenido del proyecto](#contenido-del-proyecto)
3. [Tecnologías](#tecnologías)
4. [IDE](#ide)
5. [Instalación](#instalación)
6. [Demo](#demo)
7. [Autor(es)](#autores)

***


#### Características:

  - Proyecto primer JavaScrip -> Notas previo 1
  - Captura de datos por formulario
  - Calculo simple con los datos capturados
  - Uso de API Google Charts para presentacion de datos en tabla
 
***
  #### Contenido del proyecto

| Archivo      | Descripción  |
|--------------|--------------|
| [index.html](https://gitlab.com/johnfredyb/jsnotaprevio/-/blob/main/index.html) | Archivo principal de la pagina |
| [js/app.js](https://gitlab.com/johnfredyb/jsnotaprevio/-/blob/main/JS/app.jss) | Archivo JS con sus funciones para calcular y presentar las notas del previo 1|
| [js/FuncionGoogle.js](https://gitlab.com/johnfredyb/jsnotaprevio/-/blob/main/JS/FuncionGoogle.js) | Archivo JS con sus funciones presentar las notas del previo 1 con Google Charts|

  
***
#### Tecnologías

  - [![HTML5](https://img.shields.io/badge/HTML5-CSS-green)](https://developer.mozilla.org/es/docs/Web/Guide/HTML/HTML5)
  - [![JavaScript](https://img.shields.io/badge/JavaScript-green)](https://developer.mozilla.org/es/docs/Web/JavaScript)
  - [![Google Charts](https://img.shields.io/badge/API-Google%20Charts-brightgreen)
  


Puede ver la documentacion de la API Google Charts en:

  - [Documentación Google Charts](https://developers.google.com/chart)
  
  ***
#### IDE

- El proyecto se desarrolla usando Visual Studio Code [(Visual studio Code)](https://code.visualstudio.com/docs)


***
### Instalación

Invocar página index.html


***
### Demo

Para ver el demo de la aplicación puede dirigirse a: [Primer JS -  Nota previo 1](https://johnfredyb.gitlab.io/jsnotaprevio).

***
### Autor(es)

John Fredy Buitrago B.
